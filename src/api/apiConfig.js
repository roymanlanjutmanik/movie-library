const apiConfig = {
  baseUrl: "https://api.themoviedb.org/3/",
  apiKey: "fdedfc2743b34a4dc94633225696aa66",
  originalImage: (imgPath) => `https://image.tmdb.org/t/p/original/${imgPath}`,
  w500Image: (imgPath) => `https://image.tmdb.org/t/p/w500/${imgPath}`,
};

export default apiConfig;
