import axiosClient from "./axiosClient";

export const category = {
  movie: "movie",
  tv: "tv",
};

export const movieType = {
  upcoming: "upcoming",
  popular: "popular",
  top_rated: "top_rated",
};

export const tvType = {
  popular: "popular",
  top_rated: "top_rated",
  on_the_air: "on_the_air",
};

const tmdbApi = {
  getMoviesList: (type, params) => {
    const url = "movie/" + movieType[type];
    return axiosClient.get(url, params);
  },
  getTvList: (type, params) => {
    const url = "tv/" + tvType[type];
    return axiosClient.get(url, params);
  },
  getVideos: (ctgry, id) => {
    const url = category[ctgry] + "/" + id + "/videos";
    return axiosClient.get(url, { params: {} });
  },
  search: (ctgry, params) => {
    const url = "search/" + category[ctgry];
    return axiosClient.get(url, params);
  },
  detail: (ctgry, id, params) => {
    const url = category[ctgry] + "/" + id;
    return axiosClient.get(url, params);
  },
  credits: (ctgry, id) => {
    const url = category[ctgry] + "/" + id + "/credits";
    return axiosClient.get(url, { params: {} });
  },
  similar: (cate, id) => {
    const url = category[cate] + '/' + id + '/similar';
    return axiosClient.get(url, {params: {}});
},
};

export default tmdbApi;
